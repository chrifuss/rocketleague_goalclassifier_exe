This is the precompiled version of https://gitlab.com/chrifuss/rocketleague_goalclassifier .

## How to use

Just download this project as a zip folder, unzip it anywhere you like and start the GUI.exe inside it.
Choose a Rocket League video to extract goals from, set how many frames offset you want before and after each extracted goal, and watch it work.
Be aware though that this will take a while, currently the classification runs at about 1/5 realtime.
So if you have a 10 minute video as input, expect the program to run ~50 minutes